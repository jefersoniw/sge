/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ba.sge.util;

import br.ba.sge.connection.ConnectionFactory;
import br.ba.sge.model.bean.Disciplina;
import br.ba.sge.model.bean.Matricula;
import br.ba.sge.model.bean.Pessoa;
import br.ba.sge.model.bean.Usuario;
import br.ba.sge.model.dao.DisciplinaDAO;
import br.ba.sge.model.dao.MatriculaDAO;
import br.ba.sge.model.dao.PessoaDAO;
import br.ba.sge.model.dao.UsuarioDAO;
import static br.ba.sge.view.PainelConDisc.jTableDisciplina;
import static br.ba.sge.view.PainelConMatricula.jTableMatricula;
import static br.ba.sge.view.PainelConPessoa.jTablePessoa;
import static br.ba.sge.view.PainelConUsuario.jTableUsuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Nando Cezar
 */
public class BusinessRules {

    public static String SumRevenues(String disciplina) {

        String revenues = null;
        
        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            stmt = conn.prepareStatement("SELECT SUM(mat_valorPago) FROM tb_matricula INNER JOIN tb_disciplina ON tb_disciplina.disc_codigo = tb_matricula.mat_disc_codigo WHERE tb_disciplina.disc_nome LIKE ?");
            stmt.setString(1, "%" + disciplina + "%");
            rs = stmt.executeQuery();

            if (rs.next()) {

                revenues = rs.getString("SUM(mat_valorPago)");

            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Somar Faturamento: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }
        return revenues;
    }
    
    public static boolean Controller(Disciplina disciplina) {

        int balance = 0;
        boolean controller = false;
        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {

            stmt = conn.prepareStatement("SELECT COUNT(*) AS result FROM tb_matricula INNER JOIN tb_pessoa ON tb_matricula.mat_aluno_pes_codigo = tb_pessoa.pes_codigo INNER JOIN tb_disciplina ON tb_matricula.mat_disc_codigo = tb_disciplina.disc_codigo WHERE tb_disciplina.disc_nome LIKE ?");
            stmt.setString(1, "%" + disciplina.getDisc_nome() + "%");
            rs = stmt.executeQuery();

            if (rs.next()) {

                balance = rs.getInt("result");
                
                if(balance < disciplina.getDisc_LimiteAlunos()){
                    controller = true;
                }
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Somar Faturamento: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }
        return controller;
    }

   //////////////////////////////////////////////////////////////////////////// LEITURA DE TABELA
   //////////////////////////////////////////////////////////////////////////// LEITURA DE TABELA
    public static void readJTableDisciplina() throws SQLException {

        DefaultTableModel modelo = (DefaultTableModel) jTableDisciplina.getModel();
        modelo.setNumRows(0);

        DisciplinaDAO ddao = new DisciplinaDAO();

        for (Disciplina d : ddao.Read()) {

            modelo.addRow(new Object[]{
                d.getDisc_codigo(),
                d.getDisc_nome(),
                d.getDisc_cargaHoraria(),
                d.getDisc_prof_pes_codigo(),
                d.getDisc_LimiteAlunos()});
        }
    }

    public static void readJTableMatricula() throws SQLException {

        DefaultTableModel modelo = (DefaultTableModel) jTableMatricula.getModel();
        modelo.setNumRows(0);

        MatriculaDAO mdao = new MatriculaDAO();

        for (Matricula m : mdao.Read()) {

            modelo.addRow(new Object[]{
                m.getMat_codigo(),
                m.getMat_disc_codigo(),
                m.getMat_dataMatricula(),
                m.getMat_valorPago(),
                m.getMat_aluno_pes_codigo(),
                m.getMat_periodo()});
        }
    }

    public static void readJTablePessoa() throws SQLException {

        DefaultTableModel modelo = (DefaultTableModel) jTablePessoa.getModel();
        modelo.setNumRows(0);

        PessoaDAO pdao = new PessoaDAO();

        for (Pessoa p : pdao.Read()) {

            modelo.addRow(new Object[]{
                p.getPes_codigo(),
                p.getPes_nome(),
                p.getPes_telefone(),
                p.getPes_cpf(),
                p.getPes_email()});
        }
    }

    public static void readJTableUsuario() throws SQLException {

        DefaultTableModel modelo = (DefaultTableModel) jTableUsuario.getModel();
        modelo.setNumRows(0);

        UsuarioDAO udao = new UsuarioDAO();

        for (Usuario u : udao.Read()) {

            modelo.addRow(new Object[]{
                u.getUser_matricula(),
                u.getUser_nome(),
                u.getUser_login(),
                u.getUser_senha(),
                u.getUser_email()});
        }
    }

}
