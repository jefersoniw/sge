/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.bean;

import java.math.BigDecimal;
import java.sql.Date;

/**
 *
 * @author Nando_Cezar
 */
public class Matricula {
    
    private int         mat_codigo;
    private Disciplina  mat_disc_codigo;
    private Date        mat_dataMatricula;
    private BigDecimal  mat_valorPago;
    private Pessoa      mat_aluno_pes_codigo;
    private String      mat_periodo;

    public Matricula() {}

    public Matricula(int mat_codigo, Disciplina mat_disc_codigo, Date mat_dataMatricula, BigDecimal mat_valorPago, Pessoa mat_aluno_pes_codigo, String mat_periodo) {
        this.mat_codigo = mat_codigo;
        this.mat_disc_codigo = mat_disc_codigo;
        this.mat_dataMatricula = mat_dataMatricula;
        this.mat_valorPago = mat_valorPago;
        this.mat_aluno_pes_codigo = mat_aluno_pes_codigo;
        this.mat_periodo = mat_periodo;
    }

    public int getMat_codigo() {
        return mat_codigo;
    }

    public void setMat_codigo(int mat_codigo) {
        this.mat_codigo = mat_codigo;
    }

    public Disciplina getMat_disc_codigo() {
        return mat_disc_codigo;
    }

    public void setMat_disc_codigo(Disciplina mat_disc_codigo) {
        this.mat_disc_codigo = mat_disc_codigo;
    }

    public Date getMat_dataMatricula() {
        return mat_dataMatricula;
    }

    public void setMat_dataMatricula(Date mat_dataMatricula) {
        this.mat_dataMatricula = mat_dataMatricula;
    }

    public BigDecimal getMat_valorPago() {
        return mat_valorPago;
    }

    public void setMat_valorPago(BigDecimal mat_valorPago) {
        this.mat_valorPago = mat_valorPago;
    }

    public Pessoa getMat_aluno_pes_codigo() {
        return mat_aluno_pes_codigo;
    }

    public void setMat_aluno_pes_codigo(Pessoa mat_aluno_pes_codigo) {
        this.mat_aluno_pes_codigo = mat_aluno_pes_codigo;
    }

    public String getMat_periodo() {
        return mat_periodo;
    }

    public void setMat_periodo(String mat_periodo) {
        this.mat_periodo = mat_periodo;
    }

}
