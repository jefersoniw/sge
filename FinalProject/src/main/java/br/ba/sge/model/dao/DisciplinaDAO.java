/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.dao;

import br.ba.sge.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import br.ba.sge.model.bean.Disciplina;
import br.ba.sge.model.bean.Pessoa;
import java.sql.Date;

/**
 *
 * @author Nando Cezar
 */
public class DisciplinaDAO {
    
  public void Create(Disciplina disciplina) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "INSERT INTO tb_disciplina (disc_nome, disc_cargaHoraria, disc_prof_pes_codigo, disc_limiteAlunos) "+ "values (?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, disciplina.getDisc_nome());
            stmt.setInt   (2, disciplina.getDisc_cargaHoraria());
            stmt.setInt   (3, disciplina.getDisc_prof_pes_codigo().getPes_codigo()); //Composição: Tendo acesso aos atributos da classe Pessoa;
            stmt.setInt   (4, disciplina.getDisc_LimiteAlunos());
            
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
  
  public List<Disciplina> Read() throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Disciplina> disciplinas = new ArrayList<>(); /*Lista disciplinas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_disciplina INNER JOIN tb_pessoa ON tb_pessoa.pes_codigo = tb_disciplina.disc_prof_pes_codigo");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Disciplina disciplina = new Disciplina();
                disciplina.setDisc_codigo           (rs.getInt("disc_codigo"));
                disciplina.setDisc_nome             (rs.getString("disc_nome"));
                disciplina.setDisc_cargaHoraria     (rs.getInt("disc_cargaHoraria"));
                disciplina.setDisc_LimiteAlunos     (rs.getInt("disc_limiteAlunos"));
                ////////////////////////////////////////////////////////////////////
                Pessoa pessoa = new Pessoa();
                pessoa.setPes_codigo(rs.getInt("disc_prof_pes_codigo"));
                pessoa.setPes_nome(rs.getString("pes_nome"));
                ///////////////////////////////////////////////////////////////////
                disciplina.setDisc_prof_pes_codigo(pessoa);
                
                disciplinas.add(disciplina); /*Armazenando dados da tabela 'disciplina' para lista 'disciplinas'*/
            }
            
        }catch (SQLException ex){
            System.err.println("Erro ao salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return disciplinas;
    }
  
  public List<Disciplina> ReadForNome(String nome) throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Disciplina> disciplinas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_disciplina INNER JOIN tb_pessoa ON tb_pessoa.pes_codigo = tb_disciplina.disc_prof_pes_codigo WHERE disc_nome LIKE ?");
            stmt.setString(1, "%"+nome+"%");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Disciplina disciplina = new Disciplina();
                
                disciplina.setDisc_codigo(rs.getInt("disc_codigo"));
                disciplina.setDisc_nome(rs.getString("disc_nome"));               
                disciplina.setDisc_cargaHoraria(rs.getInt("disc_cargaHoraria"));
                disciplina.setDisc_LimiteAlunos(rs.getInt("disc_limiteAlunos"));
                ////////////////////////////////////////////////////////////////////
                Pessoa pessoa = new Pessoa();
                pessoa.setPes_codigo(rs.getInt("disc_prof_pes_codigo"));
                pessoa.setPes_nome(rs.getString("pes_nome"));
                ///////////////////////////////////////////////////////////////////
                disciplina.setDisc_prof_pes_codigo(pessoa);
                
                disciplinas.add(disciplina); /*Armazenando dados da tabela 'pessoa' para lista 'pessoas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }
        return disciplinas;
    }
  
  public List<Disciplina> ReadForDate(Date date_start, Date date_end) throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Disciplina> disciplinas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM  tb_disciplina INNER JOIN tb_matricula ON tb_disciplina.disc_codigo = tb_matricula.mat_disc_codigo INNER JOIN tb_pessoa ON tb_disciplina.disc_prof_pes_codigo = tb_pessoa.pes_codigo WHERE tb_matricula.mat_dataMatricula >= ? AND tb_matricula.mat_dataMatricula <= ?");
            stmt.setDate(1, date_start);
            stmt.setDate(2, date_end);
            rs = stmt.executeQuery();
            
            while (rs.next()) {

                Disciplina disciplina = new Disciplina();
                
                disciplina.setDisc_codigo(rs.getInt("disc_codigo"));
                disciplina.setDisc_nome(rs.getString("disc_nome"));               
                disciplina.setDisc_cargaHoraria(rs.getInt("disc_cargaHoraria"));
                disciplina.setDisc_LimiteAlunos(rs.getInt("disc_limiteAlunos"));
                ////////////////////////////////////////////////////////////////////
                Pessoa pessoa = new Pessoa();
                pessoa.setPes_codigo(rs.getInt("disc_prof_pes_codigo"));
                pessoa.setPes_nome(rs.getString("pes_nome"));
                ///////////////////////////////////////////////////////////////////
                disciplina.setDisc_prof_pes_codigo(pessoa);
                
                disciplinas.add(disciplina); /*Armazenando dados da tabela 'pessoa' para lista 'pessoas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Consultar: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }
        return disciplinas;
    }

    public void Update(Disciplina disciplina) throws Exception {

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;

        try {         
            String sql = "UPDATE tb_disciplina SET disc_nome = ?, disc_cargaHoraria = ?, disc_prof_pes_codigo = ?, disc_limiteAlunos = ? WHERE disc_codigo = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, disciplina.getDisc_nome());
            stmt.setInt   (2, disciplina.getDisc_cargaHoraria());
            stmt.setInt   (3, disciplina.getDisc_prof_pes_codigo().getPes_codigo());
            stmt.setInt   (4, disciplina.getDisc_LimiteAlunos());
            stmt.setInt   (5, disciplina.getDisc_codigo());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Atualizar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }  
  
      public void Delete(Disciplina disciplina) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "DELETE FROM tb_disciplina WHERE disc_codigo = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, disciplina.getDisc_codigo());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Excluído com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
}