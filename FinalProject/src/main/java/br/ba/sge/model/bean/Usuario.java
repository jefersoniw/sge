/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.bean;

/**
 *
 * @author Nando Cezar
 */
public class Usuario {
    
    private int    user_matricula;
    private String user_nome;
    private String user_cargo;
    private String user_login;
    private String user_senha;
    private String user_email;

    public int getUser_matricula() {
        return user_matricula;
    }

    public void setUser_matricula(int user_matricula) {
        this.user_matricula = user_matricula;
    }

    public String getUser_nome() {
        return user_nome;
    }

    public void setUser_nome(String user_nome) {
        this.user_nome = user_nome;
    }

    public String getUser_cargo() {
        return user_cargo;
    }

    public void setUser_cargo(String user_cargo) {
        this.user_cargo = user_cargo;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getUser_senha() {
        return user_senha;
    }

    public void setUser_senha(String user_senha) {
        this.user_senha = user_senha;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }
    
}
