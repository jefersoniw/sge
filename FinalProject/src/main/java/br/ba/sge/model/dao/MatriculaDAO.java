/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.dao;

import br.ba.sge.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import br.ba.sge.model.bean.Disciplina;
import br.ba.sge.model.bean.Matricula;
import br.ba.sge.model.bean.Pessoa;

/**
 *
 * @author Nando Cezar
 */
public class MatriculaDAO {
    
    public void Create(Matricula matricula) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "INSERT INTO tb_matricula (mat_disc_codigo, mat_dataMatricula, mat_valorPago, mat_aluno_pes_codigo, mat_periodo) "+ "values (?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setInt        (1, matricula.getMat_disc_codigo().getDisc_codigo());
            stmt.setDate       (2, matricula.getMat_dataMatricula());
            stmt.setBigDecimal (3, matricula.getMat_valorPago());
            stmt.setInt        (4, matricula.getMat_aluno_pes_codigo().getPes_codigo());
            stmt.setString     (5, matricula.getMat_periodo());
            
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
    
     public List<Matricula> Read() throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Matricula> matriculas = new ArrayList<>(); /*Lista matriculas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_matricula INNER JOIN tb_pessoa ON tb_pessoa.pes_codigo = tb_matricula.mat_aluno_pes_codigo INNER JOIN tb_disciplina ON tb_disciplina.disc_codigo = tb_matricula.mat_disc_codigo");
            rs = stmt.executeQuery();
            
            while(rs.next()){
         
                Matricula matricula = new Matricula();
                matricula.setMat_codigo(rs.getInt("mat_codigo"));
                ////////////////////////////////////////////////////////////////////
                Disciplina disciplina = new Disciplina();
                disciplina.setDisc_codigo(rs.getInt("mat_disc_codigo"));
                disciplina.setDisc_nome(rs.getString("disc_nome"));

                matricula.setMat_disc_codigo(disciplina);
                ///////////////////////////////////////////////////////////////////  
                matricula.setMat_dataMatricula(rs.getDate("mat_dataMatricula"));
                matricula.setMat_valorPago(rs.getBigDecimal("mat_valorPago"));
                matricula.setMat_periodo(rs.getString("mat_periodo"));
                ////////////////////////////////////////////////////////////////////
                Pessoa pessoa = new Pessoa();
                pessoa.setPes_codigo(rs.getInt("mat_aluno_pes_codigo"));
                pessoa.setPes_nome(rs.getString("pes_nome"));

                matricula.setMat_aluno_pes_codigo(pessoa);
                ///////////////////////////////////////////////////////////////////             

                matriculas.add(matricula); /*Armazenando dados da tabela 'matricula' para lista 'matriculas'*/

            }
            
        }catch (SQLException ex){
            System.err.println("Erro ao salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return matriculas;
    }       
     
      public List<Matricula> ReadForNome(String nome) throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Matricula> matriculas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_matricula INNER JOIN tb_pessoa ON tb_pessoa.pes_codigo = tb_matricula.mat_aluno_pes_codigo INNER JOIN tb_disciplina ON tb_disciplina.disc_codigo = tb_matricula.mat_disc_codigo  WHERE tb_disciplina.disc_nome LIKE ?");
            stmt.setString(1, "%"+nome+"%");
            rs = stmt.executeQuery();
            
            while (rs.next()) {

                Matricula matricula = new Matricula();
                matricula.setMat_codigo(rs.getInt("mat_codigo"));
                ////////////////////////////////////////////////////////////////////
                Disciplina disciplina = new Disciplina();
                disciplina.setDisc_codigo(rs.getInt("mat_disc_codigo"));
                disciplina.setDisc_nome(rs.getString("disc_nome"));

                matricula.setMat_disc_codigo(disciplina);
                ///////////////////////////////////////////////////////////////////  
                matricula.setMat_dataMatricula(rs.getDate("mat_dataMatricula"));
                matricula.setMat_valorPago(rs.getBigDecimal("mat_valorPago"));
                matricula.setMat_periodo(rs.getString("mat_periodo"));
                ////////////////////////////////////////////////////////////////////
                Pessoa pessoa = new Pessoa();
                pessoa.setPes_codigo(rs.getInt("mat_aluno_pes_codigo"));
                pessoa.setPes_nome(rs.getString("pes_nome"));

                matricula.setMat_aluno_pes_codigo(pessoa);
                ///////////////////////////////////////////////////////////////////             

                matriculas.add(matricula); /*Armazenando dados da tabela 'matricula' para lista 'matriculas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Pesquisar: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }
        return matriculas;
    }

    public List<Matricula> ReadForSelection(String nome) throws SQLException {

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        List<Matricula> matriculas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/

        try {

            stmt = conn.prepareStatement("SELECT * FROM tb_matricula INNER JOIN tb_pessoa ON tb_pessoa.pes_codigo = tb_matricula.mat_aluno_pes_codigo INNER JOIN tb_disciplina ON tb_disciplina.disc_codigo = tb_matricula.mat_disc_codigo WHERE tb_disciplina.disc_nome LIKE ?");
            stmt.setString(1, "%" + nome + "%");
            rs = stmt.executeQuery();

            while (rs.next()) {

                Matricula matricula = new Matricula();
                matricula.setMat_codigo(rs.getInt("mat_codigo"));
                ////////////////////////////////////////////////////////////////////
                Disciplina disciplina = new Disciplina();
                disciplina.setDisc_codigo(rs.getInt("mat_disc_codigo"));
                disciplina.setDisc_nome(rs.getString("disc_nome"));

                matricula.setMat_disc_codigo(disciplina);
                ///////////////////////////////////////////////////////////////////  
                matricula.setMat_dataMatricula(rs.getDate("mat_dataMatricula"));
                matricula.setMat_valorPago(rs.getBigDecimal("mat_valorPago"));
                matricula.setMat_periodo(rs.getString("mat_periodo"));
                ////////////////////////////////////////////////////////////////////
                Pessoa pessoa = new Pessoa();
                pessoa.setPes_codigo(rs.getInt("mat_aluno_pes_codigo"));
                pessoa.setPes_nome(rs.getString("pes_nome"));

                matricula.setMat_aluno_pes_codigo(pessoa);
                ///////////////////////////////////////////////////////////////////             

                matriculas.add(matricula); /*Armazenando dados da tabela 'matricula' para lista 'matriculas'*/

            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }
        return matriculas;
    }

    public void Update(Matricula matricula) throws Exception {

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;

        try {
            String sql = "UPDATE tb_matricula SET mat_disc_codigo = ?, mat_aluno_pes_codigo = ?, mat_periodo = ? WHERE mat_codigo = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, matricula.getMat_disc_codigo().getDisc_codigo());
            //stmt.setDate        (2, matricula.getMat_dataMatricula());
            //stmt.setBigDecimal  (3, matricula.getMat_valorPago());
            stmt.setInt(2, matricula.getMat_aluno_pes_codigo().getPes_codigo());
            stmt.setString(3, matricula.getMat_periodo());
            stmt.setInt(4, matricula.getMat_codigo());

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Atualizar: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }

    public void Delete(Matricula matricula) throws Exception {

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;

        try {
            String sql = "DELETE FROM tb_matricula WHERE mat_codigo = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, matricula.getMat_codigo());

            stmt.executeUpdate();

            JOptionPane.showMessageDialog(null, "Excluído com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir: " + ex);
        } finally {
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
}
