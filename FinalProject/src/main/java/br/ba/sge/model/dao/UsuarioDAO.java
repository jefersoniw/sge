/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.dao;

import br.ba.sge.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import br.ba.sge.model.bean.Usuario;

/**
 *
 * @author Nando Cezar
 */
public class UsuarioDAO {
       
    public void Create(Usuario usuario) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "INSERT INTO tb_usuario (user_nome, user_cargo, user_login, user_senha, user_email) "+ "values (?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, usuario.getUser_nome());
            stmt.setString(2, usuario.getUser_cargo());
            stmt.setString(3, usuario.getUser_login());
            stmt.setString(4, usuario.getUser_senha());
            stmt.setString(5, usuario.getUser_email());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!!!");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
    
    public boolean checkAuthentication(String user_login, String user_senha) throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        boolean check = false;
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_usuario WHERE user_login = ? AND user_senha = ?");
            stmt.setString(1, user_login);
            stmt.setString(2, user_senha);
            rs = stmt.executeQuery();
            
            if(rs.next()){
                
                check = true;
            }
            
        }catch (SQLException ex){
            System.err.println("Erro na autenticação: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return check;
    }
    
    public List<Usuario> Read() throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Usuario> usuarios = new ArrayList<>(); /*Lista usuarios vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_usuario");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Usuario usuario = new Usuario();
                
                usuario.setUser_matricula(rs.getInt("user_matricula"));
                usuario.setUser_nome(rs.getString("user_nome"));               
                usuario.setUser_cargo(rs.getString("user_cargo"));
                usuario.setUser_login(rs.getString("user_login"));
                usuario.setUser_senha(rs.getString("user_senha"));
                usuario.setUser_email(rs.getString("user_email"));
                
                usuarios.add(usuario); /*Armazenando dados da tabela 'usuario' para lista 'usuarios'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return usuarios;
    }   
    
    public List<Usuario> ReadForNome(String nome) throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Usuario> usuarios = new ArrayList<>(); /*Lista usuarios vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_usuario WHERE user_nome LIKE ?");
            stmt.setString(1, "%"+nome+"%");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Usuario usuario = new Usuario();
                
                usuario.setUser_matricula(rs.getInt("user_matricula"));
                usuario.setUser_nome (rs.getString("user_nome"));               
                usuario.setUser_cargo(rs.getString("user_cargo"));
                usuario.setUser_login(rs.getString("user_login"));
                usuario.setUser_senha(rs.getString("user_senha"));
                usuario.setUser_email(rs.getString("user_email"));
                
                usuarios.add(usuario); /*Armazenando dados da tabela 'usuario' para lista 'usuarios'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return usuarios;
    }   
    
    public void Update(Usuario usuario) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "UPDATE tb_usuario SET user_nome = ?, user_cargo = ?, user_login = ?, user_senha = ?, user_email = ? WHERE user_matricula = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, usuario.getUser_nome());
            stmt.setString(2, usuario.getUser_cargo());
            stmt.setString(3, usuario.getUser_login());
            stmt.setString(4, usuario.getUser_senha());
            stmt.setString(5, usuario.getUser_email());
            stmt.setInt   (6, usuario.getUser_matricula());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Atualizar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
    
    public void Delete(Usuario usuario) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "DELETE FROM tb_usuario WHERE user_matricula = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, usuario.getUser_matricula());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Excluído com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
}
