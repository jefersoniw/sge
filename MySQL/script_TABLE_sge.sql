CREATE DATABASE sgebd
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE sgebd;

/*DDL - Data Definition Language*/
CREATE TABLE tb_usuario (
	
    user_matricula INT AUTO_INCREMENT,
    user_nome 	   VARCHAR(100),
	user_cargo     VARCHAR(30),
    user_login     VARCHAR(10),
    user_senha	   VARCHAR(6),
    user_email	   VARCHAR(100),
    
    PRIMARY KEY (user_matricula)

);

CREATE TABLE tb_pessoa (
	
	pes_codigo	 	INT AUTO_INCREMENT,    
	pes_nome		VARCHAR(50) ,
    pes_endereco 	VARCHAR(50) , 
    pes_uf 			VARCHAR(2)  ,
	pes_telefone 	VARCHAR(15) ,
    pes_cpf 		VARCHAR(15) ,
    pes_email 		VARCHAR(50) ,
    pes_cargo 		VARCHAR(20) ,
    
	PRIMARY KEY (pes_codigo)
  
) ;

CREATE TABLE tb_disciplina (
	
	disc_codigo  		 INT AUTO_INCREMENT,    
	disc_nome			 VARCHAR (200) ,   
    disc_cargaHoraria 	 INT  , 
    disc_prof_pes_codigo INT  ,
	disc_limiteAlunos 	 INT  ,
    
	PRIMARY KEY (disc_codigo),

	FOREIGN KEY (disc_prof_pes_codigo) REFERENCES tb_pessoa (pes_codigo)
  
) ;

CREATE TABLE tb_matricula (

  mat_codigo	   		INT (11) AUTO_INCREMENT,
  mat_disc_codigo 		INT  ,
  mat_dataMatricula 	DATE ,
  mat_valorPago 		DECIMAL (6,2) ,
  mat_aluno_pes_codigo  INT  ,
  mat_periodo 	 		VARCHAR (50)  ,
  
  PRIMARY KEY (mat_codigo),
  
  FOREIGN KEY (mat_aluno_pes_codigo) REFERENCES tb_pessoa (pes_codigo),
  FOREIGN KEY (mat_disc_codigo) 	 REFERENCES tb_disciplina (disc_codigo)
  
) ;
