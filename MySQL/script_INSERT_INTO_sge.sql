
/*DML - Data Manipulation Language*/

INSERT INTO tb_usuario 
(user_matricula, user_nome, user_cargo, user_login, user_senha, user_email) 
/*Definindo sequencialmente os campos da tabela.*/
/*Com isso, necessariamente não precisa colocar os campos entre parenteses, podendo colocar INSERT INTO disciplina VALUES direto*/

VALUES 
(DEFAULT,"Teste","Teste", "Teste", "Teste", "123", "t@teste.com"); 
/*Inserindo linhas na tabela*/
/*Para inserir linhas, basta colocar ',' e inserir mais dados*/

SELECT * FROM disciplina; /*Selecionando toda a tabela para exibição*/

INSERT INTO tb_pessoa 
(pes_codigo, pes_nome, pes_endereco, pes_uf, pes_telefone, pes_cpf, pes_email, pes_cargo, pes_login, pes_senha, pes_confSenha) 
/*Definindo sequencialmente os campos da tabela.*/
/*Com isso, necessariamente não precisa colocar os campos entre parenteses, podendo colocar INSERT INTO disciplina VALUES direto*/

VALUES 
(DEFAULT,"Teste", "Teste", "TS", "000", "000","Teste", "Teste", "Teste", "Teste", "Teste"); 
/*Inserindo linhas na tabela*/
/*Para inserir linhas, basta colocar ',' e inserir mais dados*/

SELECT * FROM tb_pessoa; /*Selecionando toda a tabela para exibição*/

SELECT * FROM tb_pessoa where pes_nome like "%Teste%";
-- sintaxe do comando update

UPDATE tb_pessoa SET pes_nome = "Andre Guimaraes"
WHERE user_matricula = 1;

-- sintaxe do comando delete

DELETE FROM tb_usuario WHERE user_matricula=1;